# Trust-Factor Blocklet

## What does it do?

It warns you if your selinux is not enforcing, useful when disabling it for any kind of purpose, so you don't forget to re-enable it later.

## Config to make it work

~~~
[trust-factor]
interval=20 # Could be more or less, this is optimal for normal operation, would use 40 or more for battery life.
~~~
