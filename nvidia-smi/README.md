# nVidia-smi Blocklet

## What does it do?

Prints GPU Temp reported by nvidia-smi.

## Programs required
* nvidia-settings
* nvidia-smi
* bash

### Optional dependencies
* nofify-send - for critical status alerts

## Config to make it work

~~~
[nvidia-smi]
interval=20 # Could be more or less, this is optimal for normal operation, would use 40 or more for battery life.
~~~
